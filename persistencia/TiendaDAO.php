<?php
class TiendaDAO{
    private $id;
    private $nombre;
    private $direccion;
       
    public function TiendaDAO($id = "", $nombre = "", $direccion = ""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> direccion = $direccion;
    }

    public function consultar(){
        return "select id, Nombre, Direccion
                from tienda
                where id = '" . $this -> id .  "'";
    }    
    
    public function insertar(){
        return "insert into tienda (Nombre, Direccion)
                values ('" . $this -> nombre . "', '" . $this -> direccion . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, Nombre, Cantidad, Precio, Imagen
                from producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id, Nombre, Direccion
                from tienda
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(id)
                from tienda";
    }
 
    public function editar(){
        return "update producto
                set Nombre = '" . $this -> nombre . "', Cantidad = '" . $this -> cantidad . "', precio = '" . $this -> precio . "'" . (($this -> imagen!="")?", Imagen = '" . $this -> imagen . "'":"") . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select idProducto, Nombre, Cantidad, Precio
                from producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%'";
    }
    
}

?>
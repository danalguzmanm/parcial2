<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $precio;
    private $unidades;
    private $tienda;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $precio = "", $unidades = "",$tienda= ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> unidades = $unidades;
        $this -> tienda = $tienda;
    }
    
    public function insertar(){
        return "insert into producto (Nombre, Precio, Unidades, idTienda)
                values ('" . $this -> nombre . "', '" . $this -> precio . "','" . $this -> unidades . "','" . $this -> tienda . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, Nombre, Precio, Unidades, idTienda
                from producto";
    }

    public function consultarTodosT(){
        return "select idProducto, Nombre, Precio, Unidades, idTienda
                from producto
                where idTienda = '".$this -> tienda."'";
    }

    public function consultar(){
        return "select idProducto, Nombre, Precio, Unidades
                from producto where idProducto = '".$this-> idProducto."'";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, Nombre, Precio, Unidades
                from producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadT($id){
        return "select count(idProducto)
                from producto
                where idTienda = '".$id."'";
    }

    public function consultarPaginacionT($cantidad, $pagina, $id){
        return "select idProducto, Nombre, Precio, Unidades
                from producto where idTienda = '".$id."'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from producto";
    }
 
    public function editar(){
        return "update producto
                set Nombre = '" . $this -> nombre . "', Unidades = '" . $this -> unidades . "', Precio = '" . $this -> precio . "'
                where idProducto = '" . $this -> idProducto .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select idProducto, Nombre, Cantidad, Precio
                from producto
                where Nombre like '%" . $filtro . "%' or Cantidad like '" . $filtro . "%' or Precio like '" . $filtro . "%'";
    }
    
}

?>
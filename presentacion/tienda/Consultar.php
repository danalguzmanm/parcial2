<?php
$juguete = new Tienda();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$juguetes = $juguete -> consultarPaginacion($cantidad, $pagina);
$totalRegistros = $juguete -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina); 
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white">
					<h4>Juguetes</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($juguetes) ?> de <?php echo $totalRegistros ?> registros encontrados  </div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Precio</th>
							<th>Ver productos</th>
							<th>Reporte productos</th>
						</tr>
						<?php 
						$i=1;
						foreach($juguetes as $Actual){
						    echo "<tr>";
						    echo "<td>" . $i. "</td>";
						    echo "<td>" . $Actual -> getNombre() . "</td>";
							echo "<td>" . $Actual -> getDireccion() . "</td>";
							echo "<td><a type='button' href='index.php?pid=". base64_encode("Presentacion/producto/Consultar.php")."&id=".$Actual->getId()."' class='btn'> <i class='fas fa-plus-circle'></i></a></td>";
							echo "<td><a type='button' name='" . $Actual->getId() . "' id='sumar' href='index.php?pid=" . base64_encode("Presentacion/tienda/reporteGeneral.php") . "&id=" . $Actual->getId() . "' class='btn' target='_blank'>Reporte <i class='fas fa-plus-circle'></i></a></td>";
							echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Juguete/Pagina.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
        						<?php 
        						for($i=1; $i<=$totalPaginas; $i++){
        						    if($i==$pagina){
        						        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
        						    }else{
        						        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Juguete/Pagina.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
        						    }        						            						    
        						}        						
        						?>
        						<li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Juguete/Pagina.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
					<select id="cantidad" >
						<option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
						<option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
						<option value="15" <?php echo ($cantidad==15)?"selected":"" ?>>15</option>
						<option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
					</select>
				</div>
            </div>
		</div>
	</div>
</div>

<script>
$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/tienda/Consultar.php") ?>&cantidad=" + $(this).val(); 	
	location.replace(url);
});
</script>

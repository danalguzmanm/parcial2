<?php
require('fpdf/fpdf.php');
ob_end_clean(); //    the buffer and never prints or returns anything.
ob_start(); // it starts buffering
$producto = new Producto("","","","",$_GET["id"]);
$tienda = new Tienda($_GET["id"]);
$tienda ->consultar();
$productos = $producto -> consultarTodosT();
$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Tienda ", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Productos tienda ".$tienda->getNombre(), 0, 2, "C");
$pdf -> SetFont("Courier", "B", 13);

$pdf->Ln();
$pdf->Cell(10,10,"id",1,0,'C');
$pdf->Cell(60,10,"nombre",1,0,'C');
$pdf->Cell(25,10,"unidades",1,0,'C');
$pdf->Cell(100,10,"precio",1,0,'C');
$pdf->Ln();
    $i=1;
    foreach($productos as $productoActual)
    {      
        $pdf->Cell(10,10,$i,1,0,'C');
        $pdf->Cell(60,10,$productoActual->getNombre(),1,0,'C');        
        $pdf->Cell(25,10,$productoActual->getUnidades(),1,0,'C');
        $pdf->Cell( 100, 10,$productoActual->getPrecio(), 1, 0, 'C', false );
        $pdf->Ln();
        $i++;
    }

$pdf -> Output();
ob_end_flush(); // It's printed here, because ob_end_flush "prints" what's in
              // the buffer, rather than returning it
              //     (unlike the ob_get_* functions)
?>





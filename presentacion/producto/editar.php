<?php
$p = new Producto($_GET["id"]);
$p->consultar();
$nombre = $p->getNombre();
$precio = $p->getPrecio();
$unidades = $p->getUnidades();

if (isset($_POST["crear"])) {
	$pr = new Producto($_GET["id"],$_POST["nombre"],$_POST["precio"],$_POST["unidades"]);
	$pr->editar();
}
?>
<div class="container mt-5">
	<div class="card" >
		<div class="card-header bg-info text-white" >
			Editar información
		</div>
		<div class="card-body" style="text-align: left;">
			<?php if (isset($_POST["crear"])) { ?>
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Datos editados
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
			<?php } ?>
			<form action='index.php?pid=<?php echo base64_encode("presentacion/producto/editar.php")."&id=".$_GET["id"] ?>' method="post">
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
				</div>
				<div class="form-group">
					<label>Unidades</label>
					<input type="number" name="unidades" class="form-control" min="1" value="<?php echo $unidades ?>" required>
				</div>
				<div class="form-group">
					<label>Precio</label>
					<input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
				</div>

				<button type="submit" name="crear" class="btn btn-info">Editar</button>
			</form>
		</div>
	</div>
</div>
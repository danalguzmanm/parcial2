<?php
$nombre = "";
if (isset($_POST["nombre"])) {
	$nombre = $_POST["nombre"];
}
$precio = "";
if (isset($_POST["precio"])) {
	$precio = $_POST["precio"];
}
$unidades = "";
if (isset($_POST["unidades"])) {
	$unidades = $_POST["unidades"];
}
$tienda = "";
if (isset($_POST["tienda"])) {
	$tienda = $_POST["tienda"];
}
if (isset($_POST["crear"])) {
	$p = new Producto("", $nombre, $precio,$unidades,$tienda);
	$p->insertar();
}
?>
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear producto</h4>
				</div>
				<div class="card-body" style="text-align: left;">
					<?php if (isset($_POST["crear"])) { ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							Datos ingresados
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
					<?php } ?>
					<form action='index.php?pid=<?php echo base64_encode("presentacion/producto/Insertar.php") ?>' method="post">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Unidades</label>
							<input type="number" name="unidades" class="form-control" min="1" value="" required>
						</div>
						<div class="form-group">
							<label>Precio</label>
							<input type="number" name="precio" class="form-control" min="1" value="" required>
						</div>
						<div class="form-group">
							<label>Tienda</label>
							<input type="number" name="tienda" class="form-control" min="1" value="" required>
						</div>

						<button type="submit" name="crear" class="btn btn-info">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require('fpdf/fpdf.php');
ob_end_clean(); //    the buffer and never prints or returns anything.
ob_start(); // it starts buffering
$producto = new Producto($_GET["id"]);
$producto -> consultar();
$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Tienda ", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Productos codigo ". $producto->getIdProducto(), 0, 2, "C");
$pdf -> SetFont("Courier", "B", 13);

$pdf->Ln();

$pdf -> Cell(216, 15, "Nombre: ", 0, 2, "L");
$pdf -> SetFont("Courier", "", 13);
$pdf -> Cell(216, 15, $producto->getNombre(), 0, 2, "L");
$pdf -> SetFont("Courier", "B", 13);
$pdf -> Cell(216, 15, "Unidades: ", 0, 2, "L");
$pdf -> SetFont("Courier", "", 13);
$pdf -> Cell(216, 15, $producto->getUnidades(), 0, 2, "L");
$pdf -> SetFont("Courier", "B", 13);
$pdf -> Cell(216, 15, "Precio: ", 0, 2, "L");
$pdf -> SetFont("Courier", "", 13);
$pdf -> Cell(216, 15, $producto->getPrecio(), 0, 2, "L");


$pdf -> Output();
ob_end_flush(); // It's printed here, because ob_end_flush "prints" what's in
              // the buffer, rather than returning it
              //     (unlike the ob_get_* functions)
?>